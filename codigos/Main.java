class Main {
    static public void main(String argv[]) {
        /* Inicia o parser */
        try {
            Yylex lex = new Yylex(new java.io.InputStreamReader(System.in,
                java.nio.charset.Charset.forName("UTF-8")));
            parser p = new parser(lex);
            Object result = p.parse().value;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}