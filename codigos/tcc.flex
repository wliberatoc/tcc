import java_cup.runtime.*;

%%
%cup
Letra = [A-Za-z]
Digito = [0-9]
FimdeLinha = \r|\n|\r\n
Espaco = {FimdeLinha} | [\t]
Inteiro = 0|[1-9][0-9]*
OpAtribuicao = "="
OpIgual = "=="
OpMais = "+"
OpMenos = "-"
OpMult = "*"
OpDiv = "/"
OpMod = "%"
PtoVirg = ";"
AbreParent = "("
FechaParent = ")"
Virgula = ","
DoisPontos = ":"
Ident = {Letra}({Letra}|{Digito})*
Pow = [p|P][o|O][w|W]
Sum = [s|S][u|U][m|M]
Count = [c|C][o|O][u|U][n|N][t|T]
Max = [m|M][a|A][x|X]
Min = [m|M][i|I][n|N]
Average = [a|A][v|V][e|E][r|R][a|A][g|G][e|E]

%%
{Espaco} {/* despreza */ } 

{FimdeLinha} { /* despreza */ }

{Inteiro} {
    float aux = Float.parseFloat(yytext()); 
    return new Symbol(sym.INTEIRO, new Float(aux));
}

{Pow} {
    String aux = yytext(); 
    return new Symbol(sym.POW, new String(aux));
}

{Sum} {
    String aux = yytext(); 
    return new Symbol(sym.SUM, new String(aux));
}

{Count} {
    String aux = yytext(); 
    return new Symbol(sym.COUNT, new String(aux));
}

{Max} {
    String aux = yytext(); 
    return new Symbol(sym.MAX, new String(aux));
}

{Min} {
    String aux = yytext(); 
    return new Symbol(sym.MIN, new String(aux));
}

{Average} {
    String aux = yytext(); 
    return new Symbol(sym.AVG, new String(aux));
}

{Ident} {
    String aux = yytext(); 
    return new Symbol(sym.IDENT, new String(aux));
}

{OpMais} { return new Symbol(sym.MAIS);}

{OpMenos} { return new Symbol(sym.MENOS);}

{OpMult} { return new Symbol(sym.MULT);}

{OpMod} { return new Symbol(sym.MOD);}

{OpAtribuicao} {return new Symbol(sym.ATRIBUICAO);}

{AbreParent} { return new Symbol(sym.ABREP);}

{FechaParent} { return new Symbol(sym.FECHAP);}

{OpDiv} { return new Symbol(sym.DIV);}

{Virgula} { return new Symbol(sym.VIRG);}

{DoisPontos} { return new Symbol(sym.DOISP);}

{PtoVirg} { return new Symbol(sym.PTVIRG);}

"."|"\n" {throw new Error ("Caractere Ilegal <"+ yytext()+">"); }
